package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    protected Predicate<User> predicateByLogin(final String login) {
        return user -> login.equals(user.getLogin());
    }

    protected Predicate<User> predicateByEmail(final String email) {
        return user -> email.equals(user.getLogin());
    }

    @Override
    public User findByLogin(final String login) {
        return models
                .stream()
                .filter(predicateByLogin(login))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return models
                .stream()
                .filter(predicateByEmail(email))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.get();
    }

}
