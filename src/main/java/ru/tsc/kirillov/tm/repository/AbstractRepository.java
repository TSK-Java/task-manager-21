package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.IRepository;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected List<M> models = new ArrayList<>();

    protected Predicate<M> predicateById(final String id) {
        return user -> id.equals(user.getId());
    }

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return models
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public M findOneById(final String id) {
        return models
                .stream()
                .filter(predicateById(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        if (collection == null) return;
        models.removeAll(collection);
    }

    @Override
    public M removeById(final String id) {
        Optional<M> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public M removeByIndex(final Integer index) {
        Optional<M> model = Optional.ofNullable(findOneByIndex(index));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public long count() {
        return models.size();
    }

}
