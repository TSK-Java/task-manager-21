package ru.tsc.kirillov.tm.repository;

import ru.tsc.kirillov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.kirillov.tm.model.AbstractUserOwnedModel;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    protected Predicate<M> predicateByUserId(final String userId) {
        return user -> userId.equals(user.getUserId());
    }

    protected Predicate<M> predicateByIdAndUserId(final String id, final String userId) {
        return user -> id.equals(user.getId()) && userId.equals(user.getUserId());
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        return models
                .stream()
                .filter(predicateByUserId(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        if (userId == null) return Collections.emptyList();
        return models
                .stream()
                .filter(predicateByUserId(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        if (userId == null || id == null) return false;
        return models
                .stream()
                .anyMatch(predicateByIdAndUserId(id, userId));
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return models
                .stream()
                .filter(predicateByIdAndUserId(id, userId))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final Optional<M> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final Optional<M> model = Optional.ofNullable(findOneByIndex(userId, index));
        model.ifPresent(models::remove);
        return model.get();
    }

    @Override
    public long count(final String userId) {
        return models
                .stream()
                .filter(predicateByUserId(userId))
                .count();
    }

}
