package ru.tsc.kirillov.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("Ошибка! Проект не найден.");
    }

}
