package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Отображение списка проектов.";
    }

    @Override
    public void execute() {
        System.out.println("[Список проектов]");
        System.out.println("Введите способ сортировки");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(getUserId(), sort);
        int idx = 0;
        for(final Project project: projects) {
            if (project == null)
                continue;
            System.out.println(++idx + ". " + project);
        }
    }

}
