package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public String getDescription() {
        return "Отобразить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(getUserId(), id);
        showProject(project);
    }

}
