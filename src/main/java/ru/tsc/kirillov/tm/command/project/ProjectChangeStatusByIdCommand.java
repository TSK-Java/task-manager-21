package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @Override
    public String getDescription() {
        return "Изменить статус проекта по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Изменение статуса проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusById(getUserId(), id, status);
    }

}
