package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.NumberUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-start-by-index";
    }

    @Override
    public String getDescription() {
        return "Запустить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Запуск проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        final Integer index = TerminalUtil.nextNumber();
        getProjectService().changeProjectStatusByIndex(getUserId(), NumberUtil.fixIndex(index), Status.IN_PROGRESS);
    }

}
