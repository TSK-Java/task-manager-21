package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Date;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Создание новой задачи.";
    }

    @Override
    public void execute() {
        System.out.println("[Создание задачи]");
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        System.out.println("Введите дату начала:");
        final Date dateBegin = TerminalUtil.nextDate();
        System.out.println("Введите дату окончания:");
        final Date dateEnd = TerminalUtil.nextDate();
        getTaskService().create(getUserId(), name, description, dateBegin, dateEnd);
    }

}
