package ru.tsc.kirillov.tm.command.user;

import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String getName() {
        return "registry";
    }

    @Override
    public String getDescription() {
        return "Регистрация пользователя.";
    }

    @Override
    public void execute() {
        System.out.println("[Регистрация пользователя]");
        System.out.println("Введите логин");
        final String login = TerminalUtil.nextLine();
        System.out.println("Введите пароль");
        final String password = TerminalUtil.nextLine();
        System.out.println("Введите E-mail");
        final String email = TerminalUtil.nextLine();
        getAuthService().registry(login, password, email);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
