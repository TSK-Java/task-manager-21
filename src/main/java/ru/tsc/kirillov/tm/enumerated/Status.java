package ru.tsc.kirillov.tm.enumerated;

public enum Status {

    NOT_STARTED("Не запущено"),
    IN_PROGRESS("Выполняется"),
    COMPLETED("Завершено");

    private String displayName;

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty())
            return null;
        for (Status status: values()) {
            if (status.name().equals(value))
                return status;
        }

        return null;
    }

    public static String toName(Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public String getDisplayName() {
        return displayName;
    }

}
