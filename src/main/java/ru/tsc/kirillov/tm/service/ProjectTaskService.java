package ru.tsc.kirillov.tm.service;

import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;
import ru.tsc.kirillov.tm.model.Task;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    private void bindTaskToProject(final String userId, final String projectId, final String taskId, boolean isAdd) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();

        task.setProjectId(isAdd ? projectId : null);
    }

    private void clearAllTaskInProject(final String userId, final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        taskRepository
                .findAllByProjectId(userId, projectId)
                .stream()
                .forEach(t -> taskRepository.removeById(userId, t.getId()));
    }

    @Override
    public void bindTaskToProject(final String userId, final String projectId, final String taskId) {
        bindTaskToProject(userId, projectId, taskId, true);
    }

    @Override
    public void unbindTaskToProject(final String userId, final String projectId, final String taskId) {
        bindTaskToProject(userId, projectId, taskId, false);
    }

    @Override
    public void removeProjectById(final String userId, String projectId) {
        clearAllTaskInProject(userId, projectId);
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void clear(final String userId) {
        projectRepository
                .findAll(userId)
                .stream()
                .forEach(p -> clearAllTaskInProject(userId, p.getId()));
        projectRepository.clear(userId);
    }

}
